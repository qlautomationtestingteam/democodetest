Code Challenge
===


#Instruction for you mission:#

Clone this project.  
Setup this project with libraries needed for Selenium-JAVA binding.  
Make sure you work on feature branch , not on master.  
Naming convention for the feature branch should be "<YourName-QloyalCodeTest>".  
Once you are complete push code to the feature branch only.  
Use your best practices to write code which you follow in your day to day development.  

# Your mission

 Go to URL : http://phptravels.com/demo/.  
 Go to "Administrator Back-End" section.   
 Click on link for the section.  
 Use the credentials provided on web to login.  
 verify you are on main landing page as "http://www.phptravels.net/admin".  
 Click on "Bookings" section.  
 Here you will see certain booking listings.  
 Next you need to write generic logic which will update any listing (as a parameter)to paid status( If its not already paid).  
 Once updated assert that the listing has changed to paid status.  
 Lastly you need to logout the account.  
 




 